[![build status](https://gitlab.com/davidpett/ember-cli-gitlab-ci/badges/master/build.svg)](https://gitlab.com/davidpett/ember-cli-gitlab-ci/commits/master)
[![npm version](https://badge.fury.io/js/ember-cli-gitlab-ci.svg)](https://badge.fury.io/js/ember-cli-gitlab-ci)
[![Ember Observer Score](http://emberobserver.com/badges/ember-cli-gitlab-ci.svg)](http://emberobserver.com/addons/ember-cli-gitlab-ci)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)
# ember-cli-gitlab-ci

This adds a `.gitlab-ci.yaml` file to your project and will automatically trigger a gitlab-ci build when you push your project to gitlab.

## Setup in your app
```
ember install ember-cli-gitlab-ci
```

Thanks to [@nahtnam](https://gitlab.com/u/nahtnam) for the `.gitlab-ci.yml` file
